﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            int i, j;
            for (i = 0; i < array.Length - 1; i++)
                for (j = 0; j < array.Length - 1 - i; j++)
                    if (array[j + 1] < array[j])
                        Swap(ref array[j], ref array[j + 1]);
        }
    }
}
