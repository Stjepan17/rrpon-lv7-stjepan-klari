﻿using System;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = { 1.05, 4.2, 5.5, 2.21, 3.33, 4.222, 7.17, 8.2, 100, 99.999 };
            NumberSequence nmberSeq = new NumberSequence(array);
            NumberSequence nmberSeq2 = new NumberSequence(10);
            nmberSeq2.InsertAt(0, 1.9);
            nmberSeq2.InsertAt(1, 1.8);
            nmberSeq2.InsertAt(2, 1.7);
            nmberSeq2.InsertAt(3, 1.2);
            nmberSeq2.InsertAt(4, 3.3);
            nmberSeq2.InsertAt(5, 4.3);
            nmberSeq2.InsertAt(6, 6.3);
            nmberSeq2.InsertAt(7, 0.3);
            nmberSeq2.InsertAt(8, 10);
            nmberSeq2.InsertAt(9, 0.0003);
            SortStrategy bubble = new BubbleSort();
            SortStrategy seq = new SequentialSort();
            SortStrategy combSort = new CombSort();
            nmberSeq.SetSortStrategy(bubble);
            nmberSeq2.SetSortStrategy(bubble);
            nmberSeq.Sort();
            nmberSeq2.Sort();
            Console.WriteLine(nmberSeq.ToString());
            Console.WriteLine(nmberSeq2.ToString());
            nmberSeq.SetSortStrategy(seq);
            nmberSeq2.SetSortStrategy(seq);
            Console.WriteLine(nmberSeq.ToString());
            Console.WriteLine(nmberSeq2.ToString());
            nmberSeq.SetSortStrategy(combSort);
            nmberSeq2.SetSortStrategy(combSort);
            Console.WriteLine(nmberSeq.ToString());
            Console.WriteLine(nmberSeq2.ToString());

            ISearch search = new LinearSearch(4);
            nmberSeq.SetSearch(search);
            nmberSeq.Search();
            ISearch search2 = new LinearSearch(0.3);
            nmberSeq2.SetSearch(search2);
            nmberSeq2.Search();
        }
    }
}
