﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7
{
    class LinearSearch : ISearch
    {
        private double searchedNumber;
        private int br = 0;
        public LinearSearch(double nmber)
        {
            this.searchedNumber = nmber;
        }

        public void Search(double[] array)
        {
            for (int i = 0; i < array.Length; i++) {
                if (array[i] == searchedNumber)
                {
                    br++;
                }
            }
            if (br > 0)
            {
                Console.WriteLine("Broj je uspješno pronađen!");
            }
            else { Console.WriteLine("Broj nije pronađen!"); }
        } 
    }
}
