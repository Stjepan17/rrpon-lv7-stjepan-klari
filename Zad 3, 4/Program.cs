﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad_3__4
{
    class Program
    {
        static void Main(string[] args)
        {
        
            SystemDataProvider provider = new SystemDataProvider();
            FileLogger logger = new FileLogger("prva.txt");
            FileLogger logger2 = new FileLogger("druga.txt");
            ConsoleLogger consolelogger = new ConsoleLogger();
            provider.Attach(logger);
            provider.Attach(logger2);
            provider.Attach(consolelogger);

            while (true)
            {
                provider.GetAvailableRAM();
                provider.GetCPULoad();
                System.Threading.Thread.Sleep(1000);
            }

        }
    }
}
