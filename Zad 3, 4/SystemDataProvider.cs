﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad_3__4
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        //ZADATAK 3
        //private float previousCPULoad;
        //private float previousRAMAvailable;
        //public SystemDataProvider() : base()
        //{
        //    this.previousCPULoad = this.CPULoad;
        //    this.previousRAMAvailable = this.AvailableRAM;
        //}
        //public float GetCPULoad()
        //{
        //    float currentLoad = this.CPULoad;
        //    if (currentLoad != this.previousCPULoad)
        //    {
        //        this.Notify();
        //    }
        //    this.previousCPULoad = currentLoad;
        //    return currentLoad;
        //}
        //public float GetAvailableRAM()
        //{
        //    float currentRAM = this.AvailableRAM;
        //    return currentRAM;
        //}

            //ZADATAK 4
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad < (this.previousCPULoad * 0.9) || currentLoad > (this.previousCPULoad * 1.1))
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentRAM = this.AvailableRAM;
            if (currentRAM < (this.previousRAMAvailable * 0.9) || currentRAM > (this.previousRAMAvailable * 1.1))
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentRAM;
            return currentRAM;
        }
    }
}
